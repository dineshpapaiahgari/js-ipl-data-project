const fs = require("fs");
const Papa = require("papaparse");

function csvToJson(csvFilePath) {
  const csvFile = fs.readFileSync(csvFilePath, "utf-8");

  const csvData = Papa.parse(csvFile, {
    header: true,
  }).data;

  const jsonData = JSON.stringify(csvData, null, 2);

  let finalData = JSON.parse(jsonData);
  return finalData.slice(0, finalData.length - 1);
}
const matchesData = csvToJson(
  "/home/dinesh/Mountblue/js-ipl-data-project/src/data/matches.csv"
);
const deliveriesData = csvToJson(
  "/home/dinesh/Mountblue/js-ipl-data-project/src/data/deliveries.csv"
);
module.exports = { matchesData, deliveriesData };