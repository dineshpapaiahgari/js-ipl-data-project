const {matchesData,deliveriesData}=require('../../csvToJSON.js');
const fs=require('fs');

function getNoOfMatchesWonByEachTeamPerYear(matchesData){

    let noOfMatchesWonByEachTeamPerYear={};

    for(let match of matchesData){
        let season=match['season'];
        let winnerTeam=match['winner'];
        if(winnerTeam===""){
        continue;
        }
        if(!noOfMatchesWonByEachTeamPerYear[season]){
            noOfMatchesWonByEachTeamPerYear[season]={[winnerTeam]:0};
        }
        if(!noOfMatchesWonByEachTeamPerYear[season][winnerTeam]){
            noOfMatchesWonByEachTeamPerYear[season][winnerTeam]=0;
        }
        noOfMatchesWonByEachTeamPerYear[season][winnerTeam]+=1;

    }
    fs.writeFileSync(
        "/home/dinesh/Mountblue/js-ipl-data-project/src/public/output/2-noOfMatchesWonByEachTeamPerYear.json",
        JSON.stringify(noOfMatchesWonByEachTeamPerYear),
        "utf-8",
        (error) => {
        if (error) throw error;
        }
    );
}

getNoOfMatchesWonByEachTeamPerYear(matchesData);