const {matchesData,deliveriesData}=require('../../csvToJSON.js');
const fs=require('fs');

function getTopEconomicalBowlersIn2015(matchesData,deliveriesData){

    let matchIdsOf2015=new Set();
    for(let match of matchesData){
        if(match['season']==="2015"){
            matchIdsOf2015.add(match["id"]);
        }
    }
    let bolwerRunsandBalls={}
    for(let delivery of deliveriesData){
        if(!matchIdsOf2015.has(delivery["match_id"])){
            continue;
        }
        let bowler=delivery["bowler"];
        if(!bolwerRunsandBalls[bowler]){
            bolwerRunsandBalls[bowler]={["totalRuns"]:0,["noOfBalls"]:0}
        }
        bolwerRunsandBalls[bowler]["totalRuns"]+=parseInt(delivery["total_runs"]);
        if(delivery["wide_runs"]==="0" && delivery["noball_runs"]==="0"){
            bolwerRunsandBalls[bowler]["noOfBalls"]+=1;
        }
    }
    let bowlerAndEconomy={}
    for(let bowler in bolwerRunsandBalls){
        let economy=(bolwerRunsandBalls[bowler]["totalRuns"]*6)/bolwerRunsandBalls[bowler]["noOfBalls"];
        bowlerAndEconomy[bowler]=economy;
    }
    let bowlerEconomies=Object.entries(bowlerAndEconomy);
    let topBowlerAndEconomy=bowlerEconomies.sort((a,b)=>a[1]-b[1]);
    fs.writeFileSync(
        "/home/dinesh/Mountblue/js-ipl-data-project/src/public/output/4-TopEconomicalBowlersIn2015.json",
        JSON.stringify(topBowlerAndEconomy),
        "utf-8",
        (error) => {
        if (error) throw error;
        }
    );
    //console.log(topBowlerAndEconomy.slice());
}
getTopEconomicalBowlersIn2015(matchesData,deliveriesData);