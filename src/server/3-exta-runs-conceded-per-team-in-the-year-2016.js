const {matchesData,deliveriesData}=require('../../csvToJSON.js');
const fs=require('fs');

function getExtraRunsConcededByEachTeamInTheYear2016(matchesData,deliveriesData){

    let matchIdsof2016=new Set();

    for(let match of matchesData){
        if(match["season"]==="2016"){
            matchIdsof2016.add(match["id"]);
        }
    }
    let extraRunsConcededByEachTeamInTheYear2016={};
    for(let delivery of deliveriesData){
        if(!matchIdsof2016.has(delivery["match_id"])){
            continue;
        }
        let bowlingTeam=delivery["bowling_team"];
        if(!extraRunsConcededByEachTeamInTheYear2016[bowlingTeam]){
            extraRunsConcededByEachTeamInTheYear2016[bowlingTeam]=0;
        }
        extraRunsConcededByEachTeamInTheYear2016[bowlingTeam]+=parseInt(delivery["extra_runs"]);
    }
    fs.writeFileSync(
        "/home/dinesh/Mountblue/js-ipl-data-project/src/public/output/3-extraRunsConcededByEachTeamInTheYear2016.json",
        JSON.stringify(extraRunsConcededByEachTeamInTheYear2016),
        "utf-8",
        (error) => {
        if (error) throw error;
        }
    );
}
getExtraRunsConcededByEachTeamInTheYear2016(matchesData,deliveriesData);