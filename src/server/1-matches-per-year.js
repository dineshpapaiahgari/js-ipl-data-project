const {matchesData,deliveriesData}=require('../../csvToJSON.js');
const fs=require('fs');

function getNoOfMatchesPlayedPerYear(matchesData){

    let noOfMatchesPlayedPerYear=new Map();

    for( let match of matchesData){
        let season=match['season'];
        if(noOfMatchesPlayedPerYear.has(season)){

            noOfMatchesPlayedPerYear.set(season,noOfMatchesPlayedPerYear.get(season)+1);
        }
        else{
            noOfMatchesPlayedPerYear.set(season,1);
        }
    }
    fs.writeFileSync(
        "/home/dinesh/Mountblue/js-ipl-data-project/src/public/output/1-matchePerYear.json",
        JSON.stringify(Object.fromEntries(noOfMatchesPlayedPerYear)),
        "utf-8",
        (error) => {
        if (error) throw error;
        }
    );
}

getNoOfMatchesPlayedPerYear(matchesData);